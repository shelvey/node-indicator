
var express = require('express');
var app = express.createServer(express.logger());
var strIndicator = "zero";

app.get('/flush', function(request, response) {
	strIndicator = "zero";
	response.send('Hello World!');
    });


app.get('/hit', function(request, response) {
	if(strIndicator == "zero"){
	    response.json({value:"zero"});
	    strIndicator = "one";
	}
    });


app.get('/check', function(request, response) {
	app.enable("jsonp callback");

	if(strIndicator == "zero"){
	    response.json({value:"zero"});
	} else if(strIndicator == "one"){
	    response.json({value:"one"});
	    strIndicator = "two";
	} else {
	    response.json({value:"two"});
	    strIndicator = "zero";
	}

    });

var port = process.env.PORT || 5000;
app.listen(port, function() {
	console.log("Listening on " + port);
});